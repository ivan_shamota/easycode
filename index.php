<html>
<head>
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

	<!-- Latest compiled and minified JavaScript -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.1/jquery.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
</head>
<body>
<?php 
$db = mysqli_connect('localhost', 'root', '108', 'easycode');
$sql = "SELECT id, login, email, first_name, last_name, gender, phone FROM users";
$res = mysqli_query($db, $sql);
$i = 0;
?>
<form action="update.php" method="post">
<table class="table table-striped" id="users">
	<thead>
		<tr>
			<td>#</td>
			<td>Login</td>
			<td>Email</td>
			<td>First Name</td>
			<td>Last Name</td>
			<td>Gender</td>
			<td>Phone</td>
			<td>Delete</td>
		</tr>
	</thead>
	<tbody>
		<?php while($row = mysqli_fetch_assoc($res)) {
			$html = "<tr class='to-change' data-id='" . $row['id'] . "'>";
			$html .= "<td>" . ++$i . "</td>";
			$html .= "<td>" . $row['login'] . "</td>";
			$html .= "<td>" . $row['email'] . "</td>";
			$html .= "<td>" . $row['first_name'] . "</td>";
			$html .= "<td>" . $row['last_name'] . "</td>";
			$html .= "<td>" . $row['gender'] . "</td>";
			$html .= "<td>" . $row['phone'] . "</td>";
			
			$html .= "<td><a href='delete.php?delete=". $row['id'] . "'>x</a></td>";
			$html .= "</tr>";
			echo $html;
		} 
		?>
	</tbody>
</table>
</form>

<!-- 
<form class="form col-md-9" action="insert.php" method="POST">
	<div class="form-group">
		<input name="login" type="text" class="form-control" placeholder="Login">		
	</div>
	
	<div class="form-group">
		<input type="password" name="password" class="form-control" placeholder="Password">		
	</div>
	
	<div class="form-group">
		<input type="email" name="email" class="form-control" placeholder="Email">		
	</div>
	
	<div class="form-group">
		<input type="text" name="first_name" class="form-control" placeholder="First name">		
	</div>
	
	<div class="form-group">
		<input type="text" name="last_name" class="form-control" placeholder="Last name">		
	</div>
	
	<div class="form-group">
		<input type="number" name="phone" class="form-control" placeholder="Phone">		
	</div>
	
	<div class="form-group">
		<input type="radio" name="gender" value="F">Female
		<input type="radio" name="gender" value="M">Male
	</div>
	
	<button class="btn btn-primary pull-right">Registration</button>
</form>-->
<script>
	$(document).ready(function(){
		var html = '';
		
		html += '<td>';
		html += '</td>';
		
		html += '<td>';
		html += '<input type="text" placeholder="login" name="login">';
		html += '</td>';
		
		html += '<td>';
		html += '<input type="submit" value="Update">';
		html += '</td>';

		
		$('.to-change').on('dblclick', function(){
			var row = $(this);
			var id = row.data('id');
			console.log(id);
			row.clear;
			html += '<input type="hidden" name="id" value="' + id + '">';
			row.html(html);
		});
	});
</script>
</body>
</html>

