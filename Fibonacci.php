<?php
header('Content-Type: text/html; charset=utf-8');
/**
 * Created by PhpStorm.
 * User: olya
 * Date: 23.02.16
 * Time: 13:30
 */
echo "<h1 align='center'>Число Фибоначчи</h1>";
echo '<BR>';
function fib($n)
{
    if ($n < 3) {
        return 1;
    } else {
        return fib($n - 1) + fib($n - 2);
    }
}
$result = "<ul>";
for ($n = 1; $n <= 30; $n++) {
    $result .= '<li>';
    $result .= fib($n);
    $result .= '</li>';
}
$result .= "</ul>";
echo $result;

